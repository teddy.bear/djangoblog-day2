#! /bin/sh

# User credentials
user=admin
email=admin@example.com
password=pass

file=db/db.sqlite3

if [ -f $file ]; then
  echo "DB file exist"
  if [ -s $file ]; then
    echo "DB file > 0 bite, no actions to be taken"
  else
    echo "DB file = 0 byte, actions to be taken"
    python3 manage.py migrate
    echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell
  fi
else
  echo "No DB file, actions to be taken"
  python3 manage.py migrate
  echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell
fi

#python3 manage.py migrate
#echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell
