# For local manual use only!
build:
	docker build -t blog .
run:
	docker run -d -p 80:8000 -v db:/code/db --name blog blog
stop:
	docker stop blog
